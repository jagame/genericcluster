package com.jagame;

public class ClusterTaskResult<R> {

    private final R result;
    private final Throwable error;
    private final String processUnitName;

    public ClusterTaskResult(String processUnitName, R result, Throwable error) {
        this.processUnitName = processUnitName;
        this.result = result;
        this.error = error;
    }

    public String getProcessUnitName() {
        return processUnitName;
    }

    public R getResult() {
        return result;
    }

    public Throwable getError() {
        return error;
    }
}
