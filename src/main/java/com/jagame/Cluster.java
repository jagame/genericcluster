package com.jagame;

import java.util.Collections;
import java.util.Objects;
import java.util.Queue;
import java.util.concurrent.*;
import java.util.stream.Stream;

/**
 * Generic cluster
 *
 * @param <T> the process unit type of the cluster
 */
public class Cluster<T> {

    private static final int DEFAULT_TIMEOUT_MILLIS_WHEN_ERROR = 3500;

    private final ExecutorService executor;
    private final Queue<T> processUnits;
    private final Queue<T> punished;
    private final int numberOfTotalProcessUnits;

    private int timeoutMillisWhenError;

    @SafeVarargs
    public Cluster(T... processUnits) {
        this(Runtime.getRuntime().availableProcessors(), processUnits);
    }

    @SafeVarargs
    public Cluster(int numberOfThreads, T... processUnits) {
        if (processUnits == null || processUnits.length == 0 ) {
            throw new ExceptionInInitializerError("A cluster need some process unit");
        }
        if( Stream.of(processUnits).anyMatch(Objects::isNull) ) {
            throw new ExceptionInInitializerError("Any process unit is null");
        }

        this.executor = Executors.newWorkStealingPool(numberOfThreads);
        this.processUnits = new ConcurrentLinkedQueue<>();
        this.punished = new ConcurrentLinkedQueue<>();
        this.numberOfTotalProcessUnits = processUnits.length;
        this.timeoutMillisWhenError = DEFAULT_TIMEOUT_MILLIS_WHEN_ERROR;

        Collections.addAll(this.processUnits, processUnits);
    }

    public void setTimeoutMillisWhenError(int timeoutMillisWhenError) {
        this.timeoutMillisWhenError = timeoutMillisWhenError;
    }

    /**
     * Submit a task to the cluster.
     *
     * @param task the task
     * @param <R> the expected result type
     * @return A future ClusterTaskResult of type R
     */
    public <R> Future<ClusterTaskResult<R>> submitTask(ClusterTask<T, R> task) {
        T processUnit = pollProcessUnit();

        return CompletableFuture.supplyAsync(task.toSupplier(processUnit), executor).handle((result, exception) -> {
            executor.submit(() -> {
                T processUnitToOffer;

                if (exception != null && numberOfTotalProcessUnits > 1) {
                    // something go wrong, wait some time for offer
                    processUnitToOffer = punishProcessUnit(processUnit);
                } else {
                    processUnitToOffer = processUnit;
                }

                processUnits.offer(processUnitToOffer);
            });

            return new ClusterTaskResult<>(processUnit.toString(), result, exception);
        });
    }

    /**
     * Punish the Process Unit and return it after some time.
     * If half of the processUnits are punished, it returns the first without waiting.
     *
     * @param processUnit
     * @return
     */
    private T punishProcessUnit(T processUnit) {
        punished.offer(processUnit);

        if(punished.size() > numberOfTotalProcessUnits/2) {
            return punished.poll();
        }

        try {
            Thread.sleep(timeoutMillisWhenError);
        } catch (InterruptedException e) { // NOSONAR
            //  nothing to do
        }
        return punished.poll();
    }

    private T pollProcessUnit() {
        T processUnit;

        // we need to wait to have some process unit available:
        do {
            processUnit = processUnits.poll();
        } while (processUnit == null);

        return processUnit;
    }

}


