package com.jagame;

import java.util.function.Supplier;

public interface ClusterTask<T, R> {

    default Supplier<R> toSupplier(T processUnit) {
        return () -> this.runTask(processUnit);
    }

    R runTask(T processUnit);

}
