package com.jagame;

import org.junit.jupiter.api.Test;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;
import org.openjdk.jmh.runner.options.TimeValue;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.Socket;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

/**
 * Unit test for Cluster
 */
public class ClusterTest { // NOSONAR JHM need this class been public

    private static final LogLevel LOG_LEVEL = LogLevel.WARN;

    private static final ErrorSimulationType ERROR_SIMULATION_TYPE = ErrorSimulationType.FIXED;

    private static final int SUCCESS_RATE = 90;
    private static final int SOCKET_LOCK_MILLIS = 0; // 3000
    private static final int NUMBER_OF_FILES_TO_CREATE = 5;

    private static final int[] SOCKET_PORTS = new int[]{
            51001, 51002, 51003, 51004, 51005
    };

    private static Cluster<Socket> cluster;

    // @BeforeAll we need to comment this for benchmark
    public static void initCluster() {
        try {
            Socket[] sockets = new Socket[SOCKET_PORTS.length];

            for (int i = 0; i < sockets.length; i++) {
                sockets[i] = new Socket("localhost", SOCKET_PORTS[i]);
            }

            cluster = new Cluster<>(sockets);
        } catch (IOException e) {
            fatal(e.getMessage());
            throw new ExceptionInInitializerError(e);
        }
    }

    @Test
    void benchmark() throws RunnerException {
        Options options = new OptionsBuilder()
                .include(this.getClass().getName() + ".*")
                .mode(Mode.AverageTime)
                .warmupTime(TimeValue.seconds(1))
                .warmupIterations(6)
                .threads(1)
                .measurementIterations(6)
                .forks(1)
                .shouldFailOnError(false)
                .shouldDoGC(true)
                .build();

        new Runner(options).run();
    }

    /**
     * Resolve the task getting a 'hsm' for FDU, each file task is synchronous.
     */
    @Test
    @Benchmark
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    public void synchronousFileCreationTasksAndAsynchronousFdu() { // NOSONAR JMH need public methods
        if (cluster == null) {
            initCluster(); // we need to run this for benchmark
        }
        SocketByFduTask socketByFduTask = new SocketByFduTask(cluster);

        runSynchronously(NUMBER_OF_FILES_TO_CREATE, socketByFduTask);
    }

    @Test
    @Benchmark
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    public void asynchronousFileCreationTasksAndAsynchronousFdu() { // NOSONAR JMH need public methods
        if (cluster == null) {
            initCluster(); // we need to run this for benchmark
        }
        SocketByFduTask socketByFduTask = new SocketByFduTask(cluster);

        runAsynchronouslyAndWait(NUMBER_OF_FILES_TO_CREATE, socketByFduTask);
    }

    @Test
    @Benchmark
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    public void asynchronousFileCreationTasksAndSynchronousFdu() { // NOSONAR JMH need public methods
        if (cluster == null) {
            initCluster(); // we need to run this for benchmark
        }
        SocketByFileTask socketByFileTask = new SocketByFileTask(cluster);

        runAsynchronouslyAndWait(NUMBER_OF_FILES_TO_CREATE, socketByFileTask);
    }

    private void runAsynchronouslyAndWait(int times, FileCreationTask fileCreationTask) {
        LinkedList<CompletableFuture<Void>> tasks = new LinkedList<>();

        for (int i = 0; i < times; i++) {
            tasks.add(CompletableFuture.runAsync(fileCreationTask::taskBody));
        }

        while (!tasks.isEmpty()) {
            if (tasks.getFirst().isDone()) {
                tasks.pollFirst();
            }
        }
    }

    private void runSynchronously(int times, FileCreationTask fileCreationTask) {
        for (int i = 0; i < times; i++) {
            fileCreationTask.taskBody();
        }
    }

    static class SocketByFileTask extends FileCreationTask {

        public SocketByFileTask(Cluster<Socket> cluster) {
            super(cluster);
        }

        @Override
        protected int numberOfRetries() {
            // 5 retries by FDU = 25 total retries
            return 25;
        }

        /**
         * Resolve the task getting a single 'hsm' for all the file
         */
        @Override
        public void taskBody() {
            List<FduTask> fduTasks = Arrays.asList(
                    new FduTaskImpl(),
                    new FduTaskImpl(),
                    new FduTaskImpl(),
                    new FduTaskImpl(),
                    new FduTaskImpl()
            );

            Future<ClusterTaskResult<List<FduResult>>> futureResult = cluster.submitTask(generateFdusTask(fduTasks));

            try {
                ClusterTaskResult<List<FduResult>> taskBodyResult = futureResult.get();

                processClusterTaskResult(generateFdusTask(fduTasks), taskBodyResult).forEach(ClusterTest::info);
            } catch (InterruptedException | ExecutionException e) {
                error(e.getMessage());
                e.printStackTrace();
            }
        }

        private ClusterTask<Socket, List<FduResult>> generateFdusTask(List<FduTask> fduTasks) {
            return socket -> {
                List<FduResult> fduResults = new ArrayList<>();

                for (FduTask fduTask : fduTasks) {
                    fduResults.add(fduTask.runTask(socket));
                }

                return fduResults;
            };
        }

    }

    static class SocketByFduTask extends FileCreationTask {

        public SocketByFduTask(Cluster<Socket> cluster) {
            super(cluster);
        }

        @Override
        protected int numberOfRetries() {
            return 5;
        }

        /**
         * Resolve the task getting a 'hsm' for FDU
         */
        @Override
        public void taskBody() {
            List<FduTask> fduTasks = Arrays.asList(
                    new FduTaskImpl(),
                    new FduTaskImpl(),
                    new FduTaskImpl(),
                    new FduTaskImpl(),
                    new FduTaskImpl()
            );

            Map<FduTask, Future<ClusterTaskResult<FduResult>>> fduResults = new HashMap<>();

            for (FduTask fduTask : fduTasks) {
                fduResults.put(fduTask, cluster.submitTask(fduTask));
            }

            try {
                for (Map.Entry<FduTask, Future<ClusterTaskResult<FduResult>>> futureResultEntry : fduResults.entrySet()) {
                    info(processClusterTaskResult(futureResultEntry.getKey(), futureResultEntry.getValue().get()));
                }
            } catch (InterruptedException | ExecutionException e) {
                error(e.getMessage());
                e.printStackTrace();
            }
        }

    }

    static abstract class FileCreationTask {

        protected final Cluster<Socket> cluster;

        public FileCreationTask(Cluster<Socket> cluster) {
            this.cluster = cluster;
        }

        public abstract void taskBody();

        protected int numberOfRetries() {
            return 1;
        }

        protected final <R> R processClusterTaskResult(ClusterTask<Socket, R> task, ClusterTaskResult<R> clusterTaskResult)
                throws ExecutionException, InterruptedException {
            if (clusterTaskResult.getError() == null) {
                return clusterTaskResult.getResult();
            }

            ClusterTaskResult<R> retryResult = clusterTaskResult;
            for (int maxTries = numberOfRetries(); maxTries > 0; maxTries--) {
                retryResult = retry(task, retryResult);
                if (retryResult.getError() == null) {
                    return retryResult.getResult();
                }
            }
            throw new InterruptedException("Can't generate FDU, canceling file generation");
        }

        protected final <R> ClusterTaskResult<R> retry(ClusterTask<Socket, R> task, ClusterTaskResult<R> clusterTaskResult) throws ExecutionException, InterruptedException {
            warn(String.format("%s, trying retry\n",
                    clusterTaskResult.getError().getMessage()));

            return cluster.submitTask(task).get();
        }

    }

    static class FduTaskImpl implements FduTask {

        private final LocalTime creationTime;

        public FduTaskImpl() {
            this.creationTime = LocalTime.now();
        }

        @Override
        public FduResult runTask(Socket processUnit) {

            info("Init FDU task with process unit " + processUnit);
            try {
                // You can't close an OutputStream without closing his socket,
                // on other words, don't close the OutputStream of the socket.
                Writer fw = new OutputStreamWriter(processUnit.getOutputStream());
                fw.write("Hey, listen!");

                throwRandomFail(processUnit);

                Thread.sleep(SOCKET_LOCK_MILLIS); // NOSONAR
            } catch (InterruptedException | IOException e) {
                throw new RuntimeException(e);
            }
            return new FduResult(creationTime, LocalTime.now());
        }

        private void throwRandomFail(Socket processUnit) throws IOException {
            if (isRandomFail(processUnit)) {
                // Error simulation
                throw new IOException("Error generating FDU using the server " + processUnit);
            }
        }

        private boolean isRandomFail(Socket processUnit) {
            switch (ERROR_SIMULATION_TYPE) {
                case FIXED:
                    return isAFixedFail(processUnit);
                case INTERMITTENT:
                    return isAIntermittentFail();
                default:
                    return false;
            }
        }

        private boolean isAIntermittentFail() {
            return new Random().nextInt(100) >= SUCCESS_RATE;
        }

        private boolean isAFixedFail(Socket processUnit) {
            return processUnit.getPort() == SOCKET_PORTS[0];
        }
    }

    interface FduTask extends ClusterTask<Socket, FduResult> {

    }

    static class FduResult {

        private final LocalTime initCreationTime;
        private final LocalTime endCreationTime;

        public FduResult(LocalTime initCreationTime, LocalTime endCreationTime) {
            this.initCreationTime = initCreationTime;
            this.endCreationTime = endCreationTime;
        }

        public LocalTime getInitCreationTime() {
            return initCreationTime;
        }

        public LocalTime getEndCreationTime() {
            return endCreationTime;
        }

        public long durationOnMillis() {
            return (endCreationTime.toNanoOfDay() - initCreationTime.toNanoOfDay()) / 1000000;
        }

        @Override
        public String toString() {
            return String.format("FduResult {\n" +
                            "\tinitCreationTime: %s,\n" +
                            "\tendCreationTime: %s\n" +
                            "\tdurationOnMillis: %d\n" +
                            "}",
                    getInitCreationTime().format(DateTimeFormatter.ISO_LOCAL_TIME),
                    getEndCreationTime().format(DateTimeFormatter.ISO_LOCAL_TIME),
                    durationOnMillis());
        }
    }

    /**
     * Helper log method for info messages
     *
     * @param message the message
     */
    public static void info(Object message) {
        log(LogLevel.INFO, message);
    }

    /**
     * Helper log method for warning messages
     *
     * @param message the message
     */
    public static void warn(Object message) {
        log(LogLevel.WARN, message);
    }

    /**
     * Helper log method for error messages
     *
     * @param message the message
     */
    public static void error(Object message) {
        log(LogLevel.ERROR, message);
    }

    /**
     * Helper log method for fatal messages
     *
     * @param message the message
     */
    public static void fatal(Object message) {
        log(LogLevel.FATAL, message);
    }

    /**
     * Helper log method
     *
     * @param logLevel the log level of the message
     * @param message the message
     */
    public static void log(LogLevel logLevel, Object message) {
        if (logLevel.ordinal() >= LOG_LEVEL.ordinal()) {
            System.out.printf("%s - %s > %s",
                    LocalTime.now().format(DateTimeFormatter.ISO_LOCAL_TIME),
                    logLevel,
                    message);
        }
    }

    enum LogLevel {
        INFO,
        WARN,
        ERROR,
        FATAL
    }

    enum ErrorSimulationType {
        FIXED, INTERMITTENT
    }
}
